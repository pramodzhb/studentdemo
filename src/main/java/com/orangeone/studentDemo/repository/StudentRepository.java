package com.orangeone.studentDemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orangeone.studentDemo.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

}
