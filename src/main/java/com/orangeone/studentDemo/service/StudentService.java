package com.orangeone.studentDemo.service;

import java.util.Optional;

import com.orangeone.studentDemo.model.Student;

public interface StudentService {

	Optional<Student> getStudentById(Integer id);

}
