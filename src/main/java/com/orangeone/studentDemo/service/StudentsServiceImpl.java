package com.orangeone.studentDemo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orangeone.studentDemo.model.Student;
import com.orangeone.studentDemo.repository.StudentRepository;

@Service
public class StudentsServiceImpl implements StudentService {
	
	@Autowired
	private StudentRepository studentRepository;

	@Override
	public Optional<Student> getStudentById(Integer id) {
		return studentRepository.findById(id);
	}

}
