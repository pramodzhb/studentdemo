package com.orangeone.studentDemo.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.orangeone.studentDemo.model.Student;
import com.orangeone.studentDemo.service.StudentService;

@RestController
@RequestMapping(value = "/student")
public class StudentController {
	@Autowired
	private StudentService studentService;

	@GetMapping(value = "/get/{id}")
	public Optional<Student> getStudent(@PathVariable Integer id) {
		return studentService.getStudentById(id);
	}
}
